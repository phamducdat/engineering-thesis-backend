package com.hust.datpd.engineeringthesis.controller;

import com.hust.datpd.engineeringthesis.dto.RealmDto;
import com.hust.datpd.engineeringthesis.service.keycloak.KeycloakService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin("*")
@RestController
@RequestMapping("/admin")
public class KeycloakController {

    final
    KeycloakService keycloakService;

    public KeycloakController(KeycloakService keycloakService) {
        this.keycloakService = keycloakService;
    }


    @PostMapping("/realms")
    public ResponseEntity<?> createRealm(@RequestBody RealmDto realmDto) {
        keycloakService.createRealm(realmDto.getRealmName());
        return ResponseEntity.ok(null);

    }
}
