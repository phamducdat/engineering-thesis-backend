package com.hust.datpd.engineeringthesis.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RegistrationRes {
    private String realm;
}
